//
//  SATScoreRepository.swift
//  JPMCSchoolChallenge
//
//  Created by Kenneth Welbeck on 8/30/23.
//

import Foundation

protocol SATRepositoryAction{
    func getDetails(dbn:String) async throws -> [SATScore]
}

class SATRepository{
    var networkManager: NetworkableProtocol
    
    init(networkManager:NetworkableProtocol){
        self.networkManager = networkManager
    }
}

extension SATRepository:SATRepositoryAction{
    func getDetails(dbn:String) async throws -> [SATScore] {
        do {
            let satRequest = SATScoreRequest(path: APIEndpoint.satScoreListEndpoint, params: ["dbn" : dbn])
            let data = try await networkManager.getDataFromAPI(urlRequest: satRequest)
            let results = try JSONDecoder().decode([SATScore].self, from: data)
            return results
        } catch {
            throw error
        }
    }
}
