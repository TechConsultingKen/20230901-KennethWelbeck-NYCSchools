//
//  SchoolDetailView.swift
//  JPMCSchoolChallenge
//
//  Created by Kenneth Welbeck on 8/30/23.
//


import SwiftUI

struct SchoolDetailView: View {
    
    let school:School
    @State var SATState:SATViewStates = .emptyView
    
    
    @StateObject var schoolVM = SchoolViewModel(repository: SchoolRepository(networkManager: NetworkManager()), repositorySAT: SATRepository(networkManager: NetworkManager()))
    
    var body: some View {
        VStack(alignment: .center) {
         
            Text(school.schoolName)
            Text("\(school.phoneNumber ?? "No Number Found" ) ")
            Text("\(school.city), \(school.zip)").padding(.bottom)
            VStack {
                switch SATState {
                case .loaded:
                    SATView()
                case .emptyView:
                    noSATView()
                }
            }

    
            
        }
        .task{
            await schoolVM.getSchoolDetailFromAPI(dbn: school.dbn)
            if ((schoolVM.detailList["SATScores"]?.isEmpty) == false){
                SATState = .loaded
            } else {
                SATState = .emptyView
            }
        }
    }
    
    @ViewBuilder
    func SATView() -> some View {
        
        Text("SAT Scores")
        Text("Math: \(schoolVM.schoolSATScore?.satMathAvgScore ?? "N/A")")
        Text("Writing: \(schoolVM.schoolSATScore?.satWritingAvgScore ?? "N/A")")
        Text("Reading: \(schoolVM.schoolSATScore?.satCriticalReadingAvgScore ?? "N/A")")

    }
    
    @ViewBuilder func noSATView() -> some View {
        Text("No SAT Scores Found")
    }
}

enum SATViewStates{
    case loaded
    case emptyView
}


